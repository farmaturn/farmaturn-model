/**
 * 
 */
package util;

public class JavaClassFileFilter extends ExtensionMatchFileFilter {
	public JavaClassFileFilter() {
		super("class");
	}
}