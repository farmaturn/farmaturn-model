package util;

import java.util.List;


public abstract class RegistradorClases {
	
	private static String myClassPath;
	private static String originalClassPath;
	private static String originalCustomClassPath;
	private static JavaClassFinder classFinder;
	public static  <T> List<? extends T> ObtenerImplementaciones(T clase) {
		
		setUp();
				
		
		tearDown();
		
		return null;
				
	}
	
	private static void setUp() {
		
		myClassPath = FileUtils.determineClassPathBase(RegistradorClases.class);
		classFinder = new JavaClassFinder();
		originalClassPath = System.getProperty("java.class.path");
		originalCustomClassPath = System.getProperty(JavaClassFinder.CUSTOM_CLASS_PATH_PROPERTY);
	}
	
	private static void tearDown() {
		System.setProperty("java.class.path", originalClassPath);
		if (originalCustomClassPath != null) {
			System.setProperty(JavaClassFinder.CUSTOM_CLASS_PATH_PROPERTY, originalCustomClassPath);
		} else {
			System.clearProperty(JavaClassFinder.CUSTOM_CLASS_PATH_PROPERTY);
		}
		System.getProperties().remove(JavaClassFinder.CUSTOM_CLASS_PATH_PROPERTY);
	}

}
