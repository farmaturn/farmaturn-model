package util;

import java.io.File;

import parser.ParserNull;

public interface FileNameUtils {
	
	public static boolean exists(String filename)
	{
		return new File(filename).exists();
	}
	
	public static String getFileExtension(String file) {
		int lastIndexOf = file.lastIndexOf('.');
		if (lastIndexOf == -1) {
			return ""; 
		}
		return file.substring(lastIndexOf).toUpperCase();
	}

}
