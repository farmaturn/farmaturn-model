package cargador;

public interface ICargador {

	public <T> T cargar(String file, T tipoDato);
	
	public boolean puedoCargar(String file);

}
