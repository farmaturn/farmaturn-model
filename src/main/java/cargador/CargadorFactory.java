package cargador;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import util.CustomClassLoader;
import util.JavaClassFinder;

public abstract class CargadorFactory {
	private static JavaClassFinder classFinder = new JavaClassFinder();
	private static List<Class<? extends ICargador>> cargadoresConocidos = new ArrayList<>();
	
	public static ICargador getCargador() {
		cargadoresConocidos = classFinder.findAllMatchingTypes(ICargador.class);
		if(cargadoresConocidos.isEmpty()) return null;
		return new CargadorProxy(cargadoresConocidos);

	}
	
}
