package cargador;

import parser.IParser;
import parser.ParserFactory;

public class CargadorTxt implements ICargador {

	@Override
	public <T> T cargar(String file, T tipoDato) {
		IParser parser = ParserFactory.getParser(file);
		return parser.parsear(file, tipoDato);
	}

	@Override
	public boolean puedoCargar(String file) {
		return ParserFactory.puedoCargar(file);
	}

}
