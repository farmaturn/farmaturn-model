package cargador;
import java.util.List;


	public class CargadorProxy implements ICargador {

	private List<Class<? extends ICargador>> cargadores;
	
	public CargadorProxy(List<Class<? extends ICargador>> cargadoresConocidos) {
		cargadores = cargadoresConocidos;
	}

	@Override
	public <T> T cargar(String file, T tipoDato) {
		for( Class<? extends ICargador> cargador : cargadores) {
			ICargador c = null;
			try {
				Object cInstance = cargador.newInstance();
				c = (ICargador) cInstance;
				if( c.puedoCargar(file))
					return c.cargar(file, tipoDato);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}

	@Override
	public boolean puedoCargar(String file) {
		return true;
	}

}
