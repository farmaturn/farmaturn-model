package historia1;

import java.util.ArrayList;
import java.util.List;

import model.Farmacia;
import model.TurnoFarmacias;

public class CargadorCSV<T> {
	
	private ParserCSV parser = new ParserCSV();
	
	public List<T> cargar(T clazz, String filename) {
		List<T> lista = new ArrayList<>();
		
		lista = parser.parsearArchivo(clazz, filename);
		
		return lista;
	}
	
	
}
