package historia1;

import java.util.List;

import model.Farmacia;
import model.TurnoFarmacias;

public class Historia1Main {

	public static void main(String[] args) {
		
		Cargador cargador = new Cargador();
		
		System.out.println("CSV\n");
		System.out.println("Farmacias");
		List<Farmacia> farmacias = cargador.cargarCSV(Farmacia.class, "farmacias.csv");
		for (Farmacia farmacia : farmacias) {
			System.out.println(farmacia);
		}
		System.out.println("\nTurnos");
		List<TurnoFarmacias> turnos = cargador.cargarCSV(TurnoFarmacias.class, "turnos.csv");
		for (TurnoFarmacias turno : turnos) {
			System.out.println(turno);
		}
		
		System.out.println("JSON\n");
		System.out.println("Farmacias");
		List<Farmacia> farmaciasJson = cargador.cargarJSON(Farmacia.class, "farmacias.json");
		for (Farmacia farmacia : farmaciasJson) {
			System.out.println(farmacia);
		}
		System.out.println("\nTurnos");
		List<TurnoFarmacias> turnosJson = cargador.cargarJSON(TurnoFarmacias.class, "turnos.json");
		for(TurnoFarmacias turno : turnosJson) {
			System.out.println(turno);
		}
		
		farmacias = cargador.cargarCSV(Farmacia.class, "farmacias.json");
		for (Farmacia farmacia : farmacias) {
			System.out.println(farmacia);
		}
	}
}
