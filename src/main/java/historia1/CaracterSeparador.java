package historia1;

public enum CaracterSeparador {
	
	COMA(','),
	PUNTOCOMA(';'),
	TAB('\t'),
	DOBLEPUNTO(':'),
	VERTICALBAR('|');
	
	private char separator;
		
	CaracterSeparador(char separator) {
		this.separator = separator;
	}
	
	public char getSeparador() {
		return this.separator;
	}
	
	public String toString() {
		return String.valueOf(this.getSeparador());
	}
	
	public boolean equals(char caracter) {
		return String.valueOf(this.getSeparador()).equals(String.valueOf(caracter));
	}
}
