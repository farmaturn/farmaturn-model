package historia1;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.Direccion;
import model.Farmacia;
import model.TurnoFarmacias;

public class ParserCSV<T> {
	
	public List<T> parsearArchivo(T clazz, String filename) {
		List<T> farmacias = new ArrayList<>();
		List<List<String>> records = new ArrayList<>();
		try {
			records = new LectorCSV().leerArchivo(filename);
			for (List<String> list : records) {
				LocalDate fecha = LocalDate.parse(list.get(0));
				break;
			}
			farmacias = (List<T>) parserCSVTurnos(records);
		}
		catch(DateTimeParseException dt) {
			farmacias = (List<T>) parserCSVFarmacias(records);
		}
		catch (IOException e) {
			e.printStackTrace();
		}		
		
		return farmacias;
		
	}
	
	private List<Farmacia> parserCSVFarmacias(List<List<String>> records) {
		List<Farmacia> farmacias = new ArrayList<>();
		try {
			for (List<String> list : records) {
				Farmacia farm = new Farmacia();
				setFarmacia(farm, list, 0);				
				farmacias.add(farm);			
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return farmacias;		
	}
	
	private List<TurnoFarmacias> parserCSVTurnos(List<List<String>> records) {
		List<TurnoFarmacias> turnos = new ArrayList<>();
		try {
			turnos = getTurnosFarmacias(records);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return turnos;	
	}
	
	private List<TurnoFarmacias> getTurnosFarmacias(List<List<String>> records) {
		List<TurnoFarmacias> turnos = new ArrayList<>();
		Map<LocalDate, List<Farmacia>> mapTurnos = new HashMap<>();
		for (List<String> list : records) {
			LocalDate fecha = LocalDate.parse(list.get(0));
			Farmacia farmacia = new Farmacia();
			setFarmacia(farmacia, list, 1);
			if(!mapTurnos.containsKey(fecha)) {
				List<Farmacia> farmacias = new ArrayList<>();
				farmacias.add(farmacia);
				mapTurnos.put(fecha, farmacias);
			}
			else {
				mapTurnos.get(fecha).add(farmacia);
			}
		}
		mapTurnos.forEach((k, v) -> turnos.add(new TurnoFarmacias(k, v)));
		return turnos;
	}
	
	private Integer StringToInt(String value) {
		Integer number = 0;
		try {
			number = Integer.parseInt(value);
		}
		catch(Exception e) {
			e.printStackTrace();
			number = 0;
		}
		return number;
	}
	
	private void setFarmacia(Farmacia farm, List<String> list, int index) {
		
		if(!list.isEmpty()) farm.setNombre(list.get(index));
		Direccion direccion = new Direccion();
		
		if(list.size() > index+1) direccion.setNombre(list.get(index+1));
		if(list.size() > index+2) {
			int altura = StringToInt(list.get(index+2));
			direccion.setAltura(altura);
		}
		farm.setDireccion(direccion);
						
		if(list.size() > index+3) {
			Set<String> telefonos = new HashSet<>();
			for(int i = index+3; i < list.size(); i++) {
				telefonos.add(list.get(i));
			}
			farm.setTelefonos(telefonos);
		}
	}
	
}
