package historia1;

import java.util.List;

public class Cargador<T> {
	
	public List<T> cargarCSV(Class<T> clazz, String filename) {
		return new CargadorCSV().cargar(clazz,filename);
	}
	
	public List<T> cargarJSON(T clazz, String filename) {
		return new CargadorJSON().cargar(clazz, filename);		
	}
}
