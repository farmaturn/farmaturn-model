package historia1;

import java.util.ArrayList;
import java.util.List;

import model.Farmacia;
import model.TurnoFarmacias;

public class CargadorJSON<T> {
	
	private ParserJSON parser = new ParserJSON();
	
	public List<T> cargar(T clazz, String filename) {
		List<T> lista = new ArrayList<>();
		
		lista = parser.parsearArchivo(clazz, filename);
		
//		if(clazz == Farmacia.class) {
//			lista = (List<T>) parser.parserJSONFarmacias(filename);
//		}
//		
//		if(clazz == TurnoFarmacias.class) {
//			lista = (List<T>) parser.parserJSONTurnos(filename);
//		}
//			
		return lista;
	
	}

}
