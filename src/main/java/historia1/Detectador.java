package historia1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Detectador {
	
	public static CaracterSeparador getDelimitador(String filename) {
		CaracterSeparador delimiter = null;
		Map<Character,List<Integer>> mapCharacters = intializeMap();		
		
		try {
			addOcurrences(mapCharacters, filename);
			delimiter = getCaracterMasRepetido(mapCharacters);			
		}
		catch(Exception e) {
			
		}			
		
		return delimiter;
	}
	
	private static Map<Character,List<Integer>> intializeMap() {
		Map<Character,List<Integer>>  mapCharacters = new HashMap<>();
		for(CaracterSeparador caracter : CaracterSeparador.values()) {
			mapCharacters.put(caracter.getSeparador(), new ArrayList<>());			
		}
		return mapCharacters;
	}
	
	private static void addOcurrences(Map<Character,List<Integer>> mapCharacters, String filename) throws IOException {
		String line = "";
		BufferedReader br = new BufferedReader(new FileReader(filename));
	    while ((line = br.readLine()) != null) {
	    	
	    	for(CaracterSeparador caracter : CaracterSeparador.values()) {
	    		int countCaracter = line.length() - line.replace(String.valueOf(caracter.getSeparador()), "").length();
	    		mapCharacters.get(caracter.getSeparador()).add(countCaracter);
	    	}		    
	    }
		br.close();
	}
	
	private static CaracterSeparador getCaracterMasRepetido(Map<Character,List<Integer>> mapCharacters) {
		CaracterSeparador delimiter = CaracterSeparador.PUNTOCOMA;
		
		boolean seRepite = true;
		int cantidadMaxima = 0;
		int cantidadEsperada = 0;
		for(CaracterSeparador caracter : CaracterSeparador.values()) {
			
			cantidadEsperada = 0;
			seRepite = true;
			
			for(int cantidad : mapCharacters.get(caracter.getSeparador())) {
				if(cantidadEsperada == 0) {
					cantidadEsperada = cantidad;
				}
				else if (cantidadEsperada != cantidad) {
						seRepite = false;
				}
			}
			
			if(seRepite) {
				if(cantidadMaxima < cantidadEsperada) {
					cantidadMaxima = cantidadEsperada;
					delimiter = caracter;
				}					
			}
		}
		
		return delimiter;
	}
	
}
