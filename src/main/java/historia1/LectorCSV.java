package historia1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LectorCSV {
	
	public List<List<String>> leerArchivo(String filename) throws IOException {
		List<List<String>> records = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(filename));			
	    String line;
	    CaracterSeparador caracter = Detectador.getDelimitador(filename);
	    while ((line = br.readLine()) != null) {
	        String[] values = line.split(String.valueOf(caracter.getSeparador()));
	        List<String> cadenas = new ArrayList<>();
	        for(String cadena : values) {
	        	cadenas.add(cadena.trim());
	        }
	        records.add(cadenas);
	    }
		br.close();
		return records;
	}
}
