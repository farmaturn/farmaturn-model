package historia1;

import java.util.ArrayList;
import java.util.List;

import model.Farmacia;
import model.TurnoFarmacias;

public class ParserJSON<T> {
	
	public List<T> parsearArchivo(T clazz, String filename) {
		List<T> farmacias = new ArrayList<>();
		try {
			LectorJSON<T> lector = new LectorJSON<T>();
			farmacias = lector.leerJSON(clazz, filename);
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return farmacias;
		
	}
}
