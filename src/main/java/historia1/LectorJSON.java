package historia1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class LectorJSON<T> {
	
	public List<T> leerJSON(T clazz, String filename) throws FileNotFoundException {
		List<T> lista =  new ArrayList<T>();
		try {
			Gson gson = new Gson(); 
	    	BufferedReader br;
			br = new BufferedReader(new FileReader(filename));

	        JsonParser parser = new JsonParser();
	        JsonArray array = parser.parse(br).getAsJsonArray();

	        lista =  new ArrayList<T>();
	        for(final JsonElement json: array){
	            T entity = gson.fromJson(json, (Class<T>) clazz);
	            lista.add(entity);
	        }


	    } catch (Exception e) {
	    	e.printStackTrace();
	    } 

        return lista;
	}

	
	private T loadSingleEntity(Type type, Class<T> objectClass, String filename) {
		Gson gson = new Gson();
		String fileName = objectClass.getSimpleName().toLowerCase();
		T enums = null;

		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(this.getClass().getResourceAsStream("/" + fileName)));

			Type collectionType = type;
			enums = gson.fromJson(br, (java.lang.reflect.Type) collectionType);
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return enums;
	}
	    
}
