package module;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import model.Farmacia;
import model.GrupoFarmaciasTurno;

public class FarmaciaModuleImplVacio implements IFarmaciaModule {

	// USARLA PARA TEST DE USER STORY 1
	@Override
	public Map<LocalDate, GrupoFarmaciasTurno> obtenerFarmaciasTurno() {
		Map<LocalDate, GrupoFarmaciasTurno> farmaciasTurno = new HashMap<>();
		return farmaciasTurno;
	}

	// USARLA PARA TEST DE USER STORY 2
	@Override
	public Set<Farmacia> obtenerCatalogoFarmacias() {
		Set<Farmacia> catalogoFarmacias = new HashSet<>();
		return catalogoFarmacias;
	}

}
