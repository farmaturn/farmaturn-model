package module;

import java.time.LocalDate;
import java.util.Map;
import java.util.Set;

import model.Farmacia;
import model.GrupoFarmaciasTurno;

public interface IFarmaciaModule {

	Map<LocalDate, GrupoFarmaciasTurno> obtenerFarmaciasTurno();

	Set<Farmacia> obtenerCatalogoFarmacias();
}
