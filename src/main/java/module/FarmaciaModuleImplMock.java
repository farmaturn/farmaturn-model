package module;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import model.Farmacia;
import model.GrupoFarmaciasTurno;

public class FarmaciaModuleImplMock implements IFarmaciaModule {

	// USARLA PARA TEST DE USER STORY 1
	@Override
	public Map<LocalDate, GrupoFarmaciasTurno> obtenerFarmaciasTurno() {
		Map<LocalDate, GrupoFarmaciasTurno> farmaciasTurno = new HashMap<>();
		Set<Farmacia> grupoFarmaciasF = new HashSet<>();
		grupoFarmaciasF.add(new Farmacia("Buffarini", "Pte. Per�n", 3030));
		grupoFarmaciasF.add(new Farmacia("Gyntila", "San Mart�n y Amenabar", 0));
		GrupoFarmaciasTurno grupoF = new GrupoFarmaciasTurno("F", grupoFarmaciasF);
		farmaciasTurno.put(LocalDate.of(2019, 04, 20), grupoF);
		return farmaciasTurno;
	}

	// USARLA PARA TEST DE USER STORY 2
	@Override
	public Set<Farmacia> obtenerCatalogoFarmacias() {
		Set<Farmacia> catalogoFarmacias = new HashSet<>();
		catalogoFarmacias.add(new Farmacia("Del aguila", "Gral. San Mart�n", 2558, "-34.5005484,-58.6984654"));
		catalogoFarmacias.add(new Farmacia("Jativa", "Rivadavia", 2017, "-34.5069096", "-58.7025082", "011 4663-0789"));
		return catalogoFarmacias;
	}

}
