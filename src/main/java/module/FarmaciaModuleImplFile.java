package module;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import model.Farmacia;
import model.GrupoFarmaciasTurno;
import parser.IParser;
import parser.ParserFactory;

public class FarmaciaModuleImplFile implements IFarmaciaModule {

	private IParser parserFarmaciasTurno;
	private IParser parserCatalogoFarmacias;
	private String filePathFarmaciasTurno;
	private String filePathCatalogoFarmacias;

	public FarmaciaModuleImplFile(String filePathFarmaciasTurno, String filePathCatalogoFarmacias) {
		this.filePathCatalogoFarmacias = filePathCatalogoFarmacias;
		this.filePathFarmaciasTurno = filePathFarmaciasTurno;
		parserCatalogoFarmacias = ParserFactory.getParser(filePathCatalogoFarmacias);
		parserFarmaciasTurno = ParserFactory.getParser(filePathFarmaciasTurno);
	}

	@Override
	public Map<LocalDate, GrupoFarmaciasTurno> obtenerFarmaciasTurno() {
		Map<LocalDate, GrupoFarmaciasTurno> map = new HashMap<LocalDate,GrupoFarmaciasTurno>();
		return parserFarmaciasTurno.parsear(filePathFarmaciasTurno, map );
	}

	@Override
	public Set<Farmacia> obtenerCatalogoFarmacias() {
		Set<Farmacia> farmacias = new HashSet<Farmacia>();
		return parserCatalogoFarmacias.parsear(filePathCatalogoFarmacias, farmacias);
	}

}
