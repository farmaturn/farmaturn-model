package parser;


public class ParserNull implements IParser {

	@Override
	public <T> T parsear(String file, T tipoDato) {
		return null;
	}

	@Override
	public boolean puedoParsear(String fileExtension) {
		return false;
	}

}
