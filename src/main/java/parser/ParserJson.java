package parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import model.Farmacia;
import model.GrupoFarmaciasTurno;
import util.FileNameUtils;

public class ParserJson implements IParser {
	
	private class DiaFarmaciaTurno {
		public String fecha;
		public String letraGrupo;
		public List<Farmacia> farmacias;
		
		public DiaFarmaciaTurno() {
			farmacias = new ArrayList<>();
		}
	}

	@Override
	public <T> T parsear(String file, T tipoDato) {
		if(tipoDato instanceof Map) {
			Map<LocalDate, GrupoFarmaciasTurno> farmaciasDeTurno = new HashMap<>();
			if(!FileNameUtils.exists(file)) return (T) farmaciasDeTurno;
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				Type type = new TypeToken<List<DiaFarmaciaTurno>>(){}.getType();
				List<DiaFarmaciaTurno> diasFarmaciaTurno = new Gson().fromJson(br, type);
				
				for (DiaFarmaciaTurno diaFarmaciaTurno : diasFarmaciaTurno) {
					Set<Farmacia> farmacias = new HashSet<>();
					farmacias.addAll(diaFarmaciaTurno.farmacias);
					GrupoFarmaciasTurno grupo = new GrupoFarmaciasTurno(diaFarmaciaTurno.letraGrupo, farmacias);
					LocalDate fecha = LocalDate.parse(diaFarmaciaTurno.fecha);
					farmaciasDeTurno.put(fecha, grupo);				
				}
			}
			catch (Exception e) { 
				e.printStackTrace(); 
			}		
			return (T) farmaciasDeTurno;
		}
		
		if(tipoDato instanceof Set) {
			Set<Farmacia> conjuntoFarmacias = new HashSet<>();
			if(!FileNameUtils.exists(file)) return (T) conjuntoFarmacias;
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				Type collectionType = new TypeToken<List<Farmacia>>(){}.getType();
				List<Farmacia> listaFarmacia = (ArrayList<Farmacia>) new Gson().fromJson( br , collectionType);
				conjuntoFarmacias.addAll(listaFarmacia);
			}
			catch (Exception e) { 
				e.printStackTrace(); 
			}
			
			return (T) conjuntoFarmacias;
		}
		return null;
	}

	@Override
	public boolean puedoParsear(String filePath) {
		String fileExtension = FileNameUtils.getFileExtension(filePath);
		if (fileExtension.equalsIgnoreCase(".JSON")) {
			return true;
		}
		return false;		
	}

}
