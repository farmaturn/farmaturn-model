package parser;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import model.Farmacia;
import model.GrupoFarmaciasTurno;

public class ParserMock implements IParser {

	@Override
	public <T> T parsear(String file, T tipoDato) {
		if(tipoDato instanceof Map) {
			Map<LocalDate, GrupoFarmaciasTurno> farmaciasTurno = new HashMap<>();
			Set<Farmacia> grupoFarmaciasA = new HashSet<>();
			grupoFarmaciasA.add(new Farmacia("Dietrich", "Ruta 8 y 197", 0));
			grupoFarmaciasA.add(new Farmacia("Macael", "Sucre", 2359));
			GrupoFarmaciasTurno grupoA = new GrupoFarmaciasTurno("A", grupoFarmaciasA);
			farmaciasTurno.put(LocalDate.of(2019, 04, 20), grupoA);
			return (T) farmaciasTurno;
		}
		
		if( tipoDato instanceof Set) {
			Set<Farmacia> catalogoFarmacias = new HashSet<>();
			catalogoFarmacias.add(new Farmacia("Borrino", "Ing. Huergo", 3595));
			catalogoFarmacias.add(new Farmacia("ARVA", "Juramento y San Mart�n", 2110));
			return (T) catalogoFarmacias;
		}
		return null;
	}

	@Override
	public boolean puedoParsear(String fileExtension) {
		return false;
	}

}
