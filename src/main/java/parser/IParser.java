package parser;

public interface IParser {

	<T> T parsear(String filePath, T tipoDato);

	boolean puedoParsear(String filePath);

}
