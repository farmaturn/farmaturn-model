package parser;

import java.util.ArrayList;
import java.util.List;

import util.JavaClassFinder;

public  abstract class ParserFactory {
	private static JavaClassFinder classFinder = new JavaClassFinder();
	private static List<Class<? extends IParser>> parsers = new ArrayList<>();
	
	public static IParser getParser(String file) {
		parsers = classFinder.findAllMatchingTypes(IParser.class);
		for (Class<? extends IParser> p : parsers) {
			IParser parser = null;
			try {
				parser = p.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			if (parser.puedoParsear(file)) {
				return parser;
			}
		}
		return new ParserNull();
	}
	
	public static boolean puedoCargar(String file) {
		for (Class<? extends IParser> p : parsers) {
			IParser parser = null;
			try {
				parser = p.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			if (parser.puedoParsear(file)) {
				return true;
			}
		}
		return false;
	}
}
