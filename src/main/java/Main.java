import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Set;

import cargador.CargadorFactory;
import cargador.ICargador;
import model.Farmacia;

public class Main {

	public static void main(String[] args) {

		ICargador cargador = CargadorFactory.getCargador();
		Set<Farmacia> farmacias = new HashSet<Farmacia>();
		farmacias = cargador.cargar("CatalogoFarmacias.json", farmacias);
		System.out.println(farmacias.toString());
	}

}
