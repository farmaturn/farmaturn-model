package model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import module.IFarmaciaModule;

public class FarmaciasTurno implements Map<LocalDate, GrupoFarmaciasTurno> {

	private Map<LocalDate, GrupoFarmaciasTurno> farmaciasTurno;

	public FarmaciasTurno(IFarmaciaModule farmaciaModule) {
		farmaciasTurno = farmaciaModule.obtenerFarmaciasTurno();
	}

	@Override
	public void clear() {
		farmaciasTurno.clear();

	}

	@Override
	public boolean containsKey(Object key) {
		return farmaciasTurno.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return farmaciasTurno.containsValue(value);
	}

	@Override
	public Set<Entry<LocalDate, GrupoFarmaciasTurno>> entrySet() {
		return farmaciasTurno.entrySet();
	}

	@Override
	public GrupoFarmaciasTurno get(Object key) {
		return farmaciasTurno.get(key);
	}

	@Override
	public boolean isEmpty() {
		return farmaciasTurno.isEmpty();
	}

	@Override
	public Set<LocalDate> keySet() {
		return farmaciasTurno.keySet();
	}

	@Override
	public GrupoFarmaciasTurno put(LocalDate key, GrupoFarmaciasTurno value) {
		return farmaciasTurno.put(key, value);
	}

	@Override
	public void putAll(Map<? extends LocalDate, ? extends GrupoFarmaciasTurno> m) {
		farmaciasTurno.putAll(m);

	}

	@Override
	public GrupoFarmaciasTurno remove(Object key) {
		return farmaciasTurno.remove(key);
	}

	@Override
	public int size() {
		return farmaciasTurno.size();
	}

	@Override
	public Collection<GrupoFarmaciasTurno> values() {
		return farmaciasTurno.values();
	}

}
