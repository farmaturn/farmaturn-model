package model;

public class Direccion {
	private Ubicacion ubicacion;
	private String nombreCalle;
	private int altura;
	
	public Direccion() {
		this.ubicacion = new Ubicacion();
	}
	
	public Direccion(String nombreCalle, int altura) {
		this.setNombre(nombreCalle);
		this.setAltura(altura);
		this.ubicacion = new Ubicacion("");
	}

	public Direccion(String nombreCalle, int altura, String coordenadas) {
		this(nombreCalle, altura);
		this.setUbicacion(new Ubicacion(coordenadas));
	}

	public Direccion(String nombreCalle, int altura, String coordenadaX, String coordenadaY) {
		this(nombreCalle, altura);
		this.setUbicacion(new Ubicacion(coordenadaX, coordenadaY));
	}

	public Ubicacion getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getNombreCalle() {
		return nombreCalle;
	}

	public void setNombre(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return nombreCalle + " " + altura + " Geolocalización: " + ubicacion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + altura;
		result = prime * result + ((nombreCalle == null) ? 0 : nombreCalle.hashCode());
		result = prime * result + ((ubicacion == null) ? 0 : ubicacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Direccion other = (Direccion) obj;
		if (altura != other.altura)
			return false;
		if (nombreCalle == null) {
			if (other.nombreCalle != null)
				return false;
		} else if (!nombreCalle.equals(other.nombreCalle))
			return false;
		if (ubicacion == null) {
			if (other.ubicacion != null)
				return false;
		} else if (!ubicacion.equals(other.ubicacion))
			return false;
		return true;
	}

}
