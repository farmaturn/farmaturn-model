package model;

public class Ubicacion {
	private String coordenadaX;
	private String coordenadaY;
	
	public Ubicacion() {
		
	}
	
	public Ubicacion(String coordenadas) {
		String[] coords = coordenadas.split(",");
		if (coords.length == 2) {
			this.setCoordenadaX(coords[0]);
			this.setCoordenadaY(coords[1]);
		}
	}

	public Ubicacion(String coordenadaX, String coordenadaY) {
		this.coordenadaX = coordenadaX;
		this.coordenadaY = coordenadaY;
	}

	public String getCoordenadaX() {
		return coordenadaX;
	}

	public String getCoordenadaY() {
		return coordenadaY;
	}

	public void setCoordenadaX(String coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	public void setCoordenadaY(String coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

	@Override
	public String toString() {
		return coordenadaX + "," + coordenadaY;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coordenadaX == null) ? 0 : coordenadaX.hashCode());
		result = prime * result + ((coordenadaY == null) ? 0 : coordenadaY.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ubicacion other = (Ubicacion) obj;
		if (coordenadaX == null) {
			if (other.coordenadaX != null)
				return false;
		} else if (!coordenadaX.equals(other.coordenadaX))
			return false;
		if (coordenadaY == null) {
			if (other.coordenadaY != null)
				return false;
		} else if (!coordenadaY.equals(other.coordenadaY))
			return false;
		return true;
	}

}
