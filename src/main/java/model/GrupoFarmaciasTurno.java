package model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GrupoFarmaciasTurno implements Map<String, Set<Farmacia>> {

	private Map<String, Set<Farmacia>> listaFarmacias;

	public GrupoFarmaciasTurno(String letraGrupo, Set<Farmacia> farmacias) {
		listaFarmacias = new HashMap<>();
		listaFarmacias.put(letraGrupo, farmacias);
	}

	@Override
	public String toString() {
		return "GrupoFarmaciasTurno [listaFarmacias=" + listaFarmacias + "]";
	}

	@Override
	public void clear() {
		listaFarmacias.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return listaFarmacias.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return listaFarmacias.containsValue(value);
	}

	@Override
	public Set<Entry<String, Set<Farmacia>>> entrySet() {
		return listaFarmacias.entrySet();
	}

	@Override
	public Set<Farmacia> get(Object key) {
		return listaFarmacias.get(key);
	}

	@Override
	public boolean isEmpty() {
		return listaFarmacias.isEmpty();
	}

	@Override
	public Set<String> keySet() {
		return listaFarmacias.keySet();
	}

	@Override
	public Set<Farmacia> put(String key, Set<Farmacia> value) {
		return listaFarmacias.put(key, value);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Set<Farmacia>> m) {
		listaFarmacias.putAll(m);
	}

	@Override
	public Set<Farmacia> remove(Object key) {
		return listaFarmacias.remove(key);
	}

	@Override
	public int size() {
		return listaFarmacias.size();
	}

	@Override
	public Collection<Set<Farmacia>> values() {
		return listaFarmacias.values();
	}

}
