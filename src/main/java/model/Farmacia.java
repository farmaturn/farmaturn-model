package model;

import java.util.HashSet;
import java.util.Set;

public class Farmacia {

	private String nombre;
	private Direccion direccion;
	private Set<String> telefonos;
	
	public Farmacia() {
		this.direccion = new Direccion();
		this.telefonos = new HashSet<>();
	}
	
	public Farmacia(String nombre, String nombreCalle, int altura) {
		this.setNombre(nombre);
		this.setDireccion(new Direccion(nombreCalle, altura));
		this.telefonos = new HashSet<>();
	}

	public Farmacia(String nombre, String nombreCalle, int altura, String coordenadaX, String coordenadaY) {
		this.setNombre(nombre);
		this.setDireccion(new Direccion(nombreCalle, altura, coordenadaX, coordenadaY));
		this.telefonos = new HashSet<>();
	}

	public Farmacia(String nombre, String nombreCalle, int altura, String coordenadas) {
		this.setNombre(nombre);
		this.setDireccion(new Direccion(nombreCalle, altura, coordenadas));
		this.telefonos = new HashSet<>();
	}

	public Farmacia(String nombre, String nombreCalle, int altura, String coordenadaX, String coordenadaY,
			String telefono) {
		this(nombre, nombreCalle, altura, coordenadaX, coordenadaY);
		this.telefonos.add(telefono);
	}

	public Farmacia(String nombre, String nombreCalle, int altura, String coordenadaX, String coordenadaY,
			Set<String> telefonos) {
		this(nombre, nombreCalle, altura, coordenadaX, coordenadaY);
		this.telefonos.addAll(telefonos);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion.toString();
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public String getTelefonos() {
		return telefonos.toString();
	}

	public void setTelefonos(Set<String> telefonos) {
		this.telefonos = telefonos;
	}

	@Override
	public String toString() {
		return nombre + ", Direccion: " + direccion + ", Telefonos: " + telefonos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((telefonos == null) ? 0 : telefonos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Farmacia other = (Farmacia) obj;
		if (direccion == null) {
			if (other.direccion != null)
				return false;
		} else if (!direccion.equals(other.direccion))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (telefonos == null) {
			if (other.telefonos != null)
				return false;
		} else if (!telefonos.equals(other.telefonos))
			return false;
		return true;
	}

}
