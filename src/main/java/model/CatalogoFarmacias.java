package model;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import module.IFarmaciaModule;

public class CatalogoFarmacias implements Set<Farmacia> {

	private Set<Farmacia> listaFarmacias;

	public CatalogoFarmacias(IFarmaciaModule farmaciaModule) {
		listaFarmacias = farmaciaModule.obtenerCatalogoFarmacias();
	}

	@Override
	public boolean add(Farmacia farmacia) {
		return listaFarmacias.add(farmacia);
	}

	@Override
	public boolean addAll(Collection<? extends Farmacia> farmacias) {
		return listaFarmacias.addAll(farmacias);
	}

	@Override
	public void clear() {
		listaFarmacias.clear();
	}

	@Override
	public boolean contains(Object o) {
		return listaFarmacias.contains(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return listaFarmacias.containsAll(c);
	}

	@Override
	public boolean isEmpty() {
		return listaFarmacias.isEmpty();
	}

	@Override
	public Iterator<Farmacia> iterator() {
		return listaFarmacias.iterator();
	}

	@Override
	public boolean remove(Object o) {
		return listaFarmacias.remove(o);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return listaFarmacias.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return listaFarmacias.retainAll(c);
	}

	@Override
	public int size() {
		return listaFarmacias.size();
	}

	@Override
	public Object[] toArray() {
		return listaFarmacias.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return listaFarmacias.toArray(a);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((listaFarmacias == null) ? 0 : listaFarmacias.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatalogoFarmacias other = (CatalogoFarmacias) obj;
		if (listaFarmacias == null) {
			if (other.listaFarmacias != null)
				return false;
		} else if (!listaFarmacias.equals(other.listaFarmacias))
			return false;
		return true;
	}

}
