package model;

import java.time.LocalDate;
import java.util.List;

public class TurnoFarmacias {
	
	private String fecha;
	private List<Farmacia> farmacias;
	
	public TurnoFarmacias(LocalDate fecha, List<Farmacia> farmacias) {
		this.fecha = fecha.toString();
		this.farmacias = farmacias;
	}
	
	public LocalDate getFecha() {
		return LocalDate.parse(fecha);
	}
	
	public List<Farmacia> getFarmacias() {
		return this.farmacias;
	}

	@Override
	public String toString() {
		LocalDate fechaTurno = getFecha();
		String turnos = fechaTurno.getDayOfMonth() + "/" + fechaTurno.getMonthValue() + "/" + fechaTurno.getYear() + ": \n"; 
		for(Farmacia farmacia : farmacias) {
			turnos += farmacia + "\n";
		}
		return turnos;
	}
	
	
}
