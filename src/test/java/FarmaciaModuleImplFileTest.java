import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.CatalogoFarmacias;
import model.FarmaciasTurno;
import module.FarmaciaModuleImplFile;
import module.IFarmaciaModule;

public class FarmaciaModuleImplFileTest {

	private IFarmaciaModule module;

	public FarmaciaModuleImplFileTest() {
		module = new FarmaciaModuleImplFile("FarmaciasDeTurno.json", "CatalogoFarmacias.json");
	}

	@Test
	public void obtenerFarmaciasTurnoTestOK() {
		FarmaciasTurno farmacias = new FarmaciasTurno(module);
		assertTrue(farmacias.size() == 2);
	}

	@Test
	public void obtenerCatalogoFarmaciasTestOK() {
		CatalogoFarmacias catalogo = new CatalogoFarmacias(module);
		assertTrue(catalogo.size() == 2);
	}

}
