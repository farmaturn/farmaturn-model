package classes;

public class Class1 implements MyTagInterface {
	
	private int number;
	
	public Class1(int number) {
		this.number = number;
	}
	
	@Override
	public int getNumber() {
		return this.number;
	}
	
	public String toString() {
		return String.valueOf(this.number);
	}

}
