package classes;

public class Class2 implements MyTagInterface {
	
	private int number;
	
	public Class2(int number) {
		this.number = number;
	}
	
	public int getNumber() {
		return number * 2;
	}
	
	public String toString() {
		return String.valueOf(this.number);
	}
}
