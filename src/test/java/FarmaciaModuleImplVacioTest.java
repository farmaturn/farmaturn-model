import static org.junit.Assert.assertEquals;

import org.junit.Test;

import module.FarmaciaModuleImplVacio;

public class FarmaciaModuleImplVacioTest {

	@Test
	public void obtenerFarmaciasTurnoVacioTest() {
		FarmaciaModuleImplVacio f = new FarmaciaModuleImplVacio();
		assertEquals(true, f.obtenerFarmaciasTurno().toString().equals("{}"));
	}

	@Test
	public void obtenerCatalogoFarmaciasVacioTest() {
		FarmaciaModuleImplVacio f = new FarmaciaModuleImplVacio();
		assertEquals(true, f.obtenerCatalogoFarmacias().toString().equals("[]"));
	}
}
