import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import model.Farmacia;
import model.GrupoFarmaciasTurno;
import module.FarmaciaModuleImplMock;

public class FarmaciaModuleImplMockTest {

	@Test
	public void obtenerFarmaciasTurnoTest() {
		Map<LocalDate, GrupoFarmaciasTurno> farmaciasTurno = new HashMap<>();
		Set<Farmacia> grupoFarmaciasF = new HashSet<>();
		grupoFarmaciasF.add(new Farmacia("Buffarini", "Pte. Per�n", 3030));
		grupoFarmaciasF.add(new Farmacia("Gyntila", "San Mart�n y Amenabar", 0));
		GrupoFarmaciasTurno grupoF = new GrupoFarmaciasTurno("F", grupoFarmaciasF);
		farmaciasTurno.put(LocalDate.of(2019, 04, 20), grupoF);
		FarmaciaModuleImplMock f = new FarmaciaModuleImplMock();
		assertEquals(true, f.obtenerFarmaciasTurno().toString().equals(farmaciasTurno.toString()));
	}

	@Test
	public void obtenerCatalogoFarmaciasTest() {
		Farmacia f1 = new Farmacia("Del aguila", "Gral. San Mart�n", 2558, "-34.5005484,-58.6984654");
		Farmacia f2 = new Farmacia("Jativa", "Rivadavia", 2017, "-34.5069096", "-58.7025082", "011 4663-0789");
		Set<Farmacia> catalogoFarmacias = new HashSet<>();
		catalogoFarmacias.add(f1);
		catalogoFarmacias.add(f2);
		FarmaciaModuleImplMock f = new FarmaciaModuleImplMock();
		assertEquals(true, f.obtenerCatalogoFarmacias().equals(catalogoFarmacias));
	}

}
