package cargador;

import java.util.List;

import org.junit.Test;

import util.JavaClassFinder;

import static org.junit.Assert.assertEquals;

public class CargadorTest {
	private JavaClassFinder classFinder;
	
	@Test
	public void FindImplementationsTest() {
		List<Class<? extends ICargador>> classes = classFinder.findAllMatchingTypes(ICargador.class);
		assertEquals("found the class", 3, classes.size());

	}
}
