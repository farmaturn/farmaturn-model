import static org.junit.Assert.assertTrue;

import org.junit.Test;

import parser.IParser;
import parser.ParserFactory;
import parser.ParserJson;
import parser.ParserNull;

public class ParserTest {
	
	@Test
	public void parserJsonTestOk() {
		IParser parser = ParserFactory.getParser("FarmaciasDeTurno.json");
		assertTrue(parser instanceof ParserJson);
	}
	
	@Test
	public void parserNullTestOk() {
		IParser parser = ParserFactory.getParser("CatalogoFarmacias.exe");
		assertTrue(parser instanceof ParserNull);
	}
}
