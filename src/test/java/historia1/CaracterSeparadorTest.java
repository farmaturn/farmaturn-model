package historia1;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CaracterSeparadorTest {
	
	@Test
	public void caracterSeparadorPuntoComaTestOk() {
		CaracterSeparador caracter = CaracterSeparador.PUNTOCOMA;
		assertTrue(caracter.equals(caracter));
	}
	
	@Test
	public void caracterSeparadorComaTestOk() {
		CaracterSeparador caracter = CaracterSeparador.COMA;
		assertTrue(caracter.equals(caracter));
	}
}
