package historia1;


import java.io.FileNotFoundException;

import org.junit.Test;

import model.Farmacia;

public class LectorJSONTest {
	
	@Test(expected = FileNotFoundException.class)
	public void leerArchivoTestArchivoNoEncontrado() throws FileNotFoundException {
		LectorJSON lector = new LectorJSON();
		lector.leerJSON(Farmacia.class,"farmaciasTest.json");
	}
	
	@Test
	public void leerArchivoTestArchivoEncontrado() throws FileNotFoundException {
		LectorJSON lector = new LectorJSON();
		lector.leerJSON(Farmacia.class,"farmacias.json");
	}
}
