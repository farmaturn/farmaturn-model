package historia1;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import model.Farmacia;
import model.TurnoFarmacias;

public class ParserCSVTest {
	
	@Test
	public void parserCSVFarmaciasTestOk() {
		ParserCSV parser = new ParserCSV();
		List<Farmacia> farmacias = parser.parsearArchivo(Farmacia.class, "farmacias.csv");
		assertTrue(farmacias.size() > 1);
	}
	
	@Test
	public void parserCSVFarmaciasTestEmpty() {
		ParserCSV parser = new ParserCSV();
		List<Farmacia> farmacias = parser.parsearArchivo(Farmacia.class, "farmaciastest.csv");
		assertTrue(farmacias.isEmpty());
	}
	
	@Test
	public void parserCSVFarmaciasTestTipoArchivoErroneo() {
		ParserCSV parser = new ParserCSV();
		List<Farmacia> farmacias = parser.parsearArchivo(Farmacia.class, "farmacias.json");
		assertTrue(farmacias.size() > 2);
	}
	
	@Test
	public void parserCSVTurnosTestOk() {
		ParserCSV parser = new ParserCSV();
		List<TurnoFarmacias> farmacias = parser.parsearArchivo(TurnoFarmacias.class, "turnos.csv");
		assertTrue(farmacias.size() > 1);
	}
	
	@Test
	public void parserCSVTurnosTestEmpty() {
		ParserCSV parser = new ParserCSV();
		List<TurnoFarmacias> farmacias = parser.parsearArchivo(TurnoFarmacias.class, "turnostest.csv");
		assertTrue(farmacias.isEmpty());
	}
}
