package historia1;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import model.Direccion;
import model.Farmacia;
import model.TurnoFarmacias;

public class CargadorJSONTest {
	
	@Test
	public <T> void cargarFarmaciasTestOk() {
		CargadorJSON cargador = new CargadorJSON();
		List<T> lista = cargador.cargar(Farmacia.class, "farmacias.json");
		assertTrue(lista.size() > 1);
	}
	
	@Test
	public <T> void cargarTurnosTestOk() {
		CargadorJSON cargador = new CargadorJSON();
		List<T> lista = cargador.cargar(TurnoFarmacias.class, "turnos.json");
		assertTrue(lista.size() > 1);
	}
	
	@Test
	public <T> void cargarTestClaseInvalida() {
		CargadorJSON cargador = new CargadorJSON();
		List<T> lista = cargador.cargar(Direccion.class, "turnos.json");
		assertTrue(lista.isEmpty());
	}
}
