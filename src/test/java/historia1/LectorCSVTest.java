package historia1;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

public class LectorCSVTest {

	@Rule
    public ExpectedException thrown= ExpectedException.none();
	
	@Test(expected = IOException.class)
	public void leerArchivoTestArchivoNoEncontrado() throws IOException {
		LectorCSV lector = new LectorCSV();
		lector.leerArchivo("farmaciasTest.csv");
	}
	
	@Test
	public void leerArchivoTestArchivoEncontrado() throws IOException {
		LectorCSV lector = new LectorCSV();
		lector.leerArchivo("farmacias.csv");
	}
	
	@Test
	public void leerArchivoTestEncontroMasDeUnaFila() throws IOException {
		LectorCSV lector = new LectorCSV();
		List<List<String>> listas = lector.leerArchivo("farmacias.csv");
		assertTrue(listas.size() > 1);
	}
	
	@Test
	public void leerArchivoTestEncontroMasDeUnaColumna() throws IOException {
		LectorCSV lector = new LectorCSV();
		List<List<String>> listas = lector.leerArchivo("farmacias.csv");
		assertTrue(listas.get(0).size() > 1);
	}
}
