package historia1;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import model.Direccion;
import model.Farmacia;
import model.TurnoFarmacias;

public class CargadorCSVTest {
	
	@Test
	public <T> void cargarFarmaciasTestOk() {
		CargadorCSV cargador = new CargadorCSV();
		List<T> lista = cargador.cargar(Farmacia.class, "farmacias.csv");
		assertTrue(lista.size() > 1);
	}
	
	@Test
	public <T> void cargarTurnosTestOk() {
		CargadorCSV cargador = new CargadorCSV();
		List<T> lista = cargador.cargar(TurnoFarmacias.class, "turnos.csv");
		assertTrue(lista.size() > 1);
	}
	
	@Test
	public <T> void cargarTestClaseInvalida() {
		CargadorCSV cargador = new CargadorCSV();
		List<T> lista = cargador.cargar(Direccion.class, "turnos.csv");
		assertTrue(lista.isEmpty());
	}
}
