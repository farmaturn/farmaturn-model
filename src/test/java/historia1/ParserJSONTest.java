package historia1;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.google.gson.JsonSyntaxException;

import model.Farmacia;
import model.TurnoFarmacias;

public class ParserJSONTest {
	
	@Test
	public void parserJSONFarmaciasTestOk() {
		ParserJSON parser = new ParserJSON();
		List<Farmacia> farmacias = parser.parsearArchivo(Farmacia.class, "farmacias.json");
		assertTrue(farmacias.size() > 1);
	}
	
	@Test
	public void parserJSONFarmaciasTestEmpty() {
		ParserJSON parser = new ParserJSON();
		List<Farmacia> farmacias = parser.parsearArchivo(Farmacia.class, "farmaciastest.json");
		assertTrue(farmacias.isEmpty());
	}
	
	@Test
	public void parserJSONFarmaciasTestTipoArchivoErroneo() {
		ParserJSON parser = new ParserJSON();
		List<Farmacia> farmacias = parser.parsearArchivo(Farmacia.class, "farmacias.csv");
		assertTrue(farmacias.isEmpty());
	}
	
	@Test
	public void parserJSONTurnosTestOk() {
		ParserJSON parser = new ParserJSON();
		List<TurnoFarmacias> farmacias = parser.parsearArchivo(TurnoFarmacias.class, "turnos.json");
		assertTrue(farmacias.size() > 1);
	}
	
	@Test
	public void parserJSONTurnosTestEmpty() {
		ParserJSON parser = new ParserJSON();
		List<TurnoFarmacias> farmacias = parser.parsearArchivo(TurnoFarmacias.class, "turnostest.csv");
		assertTrue(farmacias.isEmpty());
	}
}
